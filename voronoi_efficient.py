# OPEN CELLULAR CONVECTION USING LAGRANGIAN TRIGGERING
# A TWO LAYER SIMPLE MODEL WHICH IMITATES CONVECTIVE BEHAVIOUR

# This is a fast implementation using weave
# Uses a double grid for calculating the nearest rain cluster
# The domain can be up to a few thousand points in each direction

# Copyright (c) 2016, University of Leeds
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of University of Leeds nor the
#   names of other contributors may be used to endorse or promote products
# derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL UNIV. OF LEEDS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# IMPORTS
use_mpl_stats=True # make stat matplotlib plots
use_mpl_simple=True # make simple matplotlib plots
use_mpl_detailed=True # make expensive maplotlib plots as well
use_pyqtgraph=True
l_color_plots=False
l_run_all_exps=False
import numpy as np
from numpy import * # Lazy import of numpy functions
import weave
from weave import converters
import os,errno,sys,shutil
import time

# Optionally use matplotlib
try:
    from matplotlib.pyplot import * # Lazy import of plotting functions
    from matplotlib.colors import Normalize
    from matplotlib.image import NonUniformImage
    from PIL import Image # Use pil to combine plots
except:
    use_mpl_simple=False
    use_mpl_detailed=False
    use_mpl_stats=False

# Optionally use pyqtgraph
try:
    from pyqtgraph.Qt import QtCore, QtGui
    import pyqtgraph as pg
except:
    use_pyqtgraph=False
       
# PLOTTING PATH
basepath='voronoi/'
plotpath=basepath+'test'
outformat='png'

# PARAMETER SETTINGS
time_steps=1000 # number of time steps
plot_every=10000 # how often to make a matplotlib plot
plot_every_detailed=10000 # how often to make a detailed matplotlib plot
size_stats_write_every=10000
plot_fracs=[1.0,0.125] # fraction of domains to plot

# PARAMETER SETTINGS
nxy_coarse=64 # number of horizontal points in each direction, coarse level grid
nxy_fine=10 # number of horizontal points in each direction, fine level grid
gridbox_seeds_per_step=0.03 # seeds per time step per gridbox
rain_delay=15 # delay before rain onset in time steps
rain_duration=25 # rain duration in time steps
div_speed_scale=1.0 # scaling for number of grid cells traversed by particle per time step
n_crit_convect_low=4 # critical number of particles per grid cell for removal, lower layer
n_crit_neighbour_low=3 # critical number for neighbour induced removal, lower layer
subdomain_frac=0.0625 # fraction of domain used for sub-domain statistics
cutoff_area_ratio=5.0 # cutoff ratio for perimeter to 4*sqrt(area)
substep_lengths=array([0.4,0.3,0.2,0.1]) # array with progressing of decreasing substeps
   
# Non-standard parameters
n_crit_convect_upp=1 # critical number of particles per grid cell for removal, upper layer
n_crit_neighbour_upp=1 # critical number for neighbour induced removal, upper layer
translate_speed_low=0 # translation speed for lower layer
translate_speed_upp=0 # translation speed for upper layer
dist_add=1 # added virtual distance, which slows down perturbation growth
minsize_low=0 # minimal size for avalanche (in particles)
minsize_end=30 # minimal size of rain cluster
diffuse_speed_low=0.0 # measure of diffusion speed in lower layer
diffuse_speed_upp=0.0 # measure of diffusion speed in upper layer
time_grow_start=0 # when to start increasing minsize
time_to_grow=1 # time it takes to increase minsize
random_seed=42 # initial random seed for reproducable random number generation
np.random.seed(random_seed)
l_disturb_run=False # disturb run at some point in time

# INITIALISATION
nxy_total=nxy_coarse*nxy_fine # number of n_grid_low cells in each direction
nxy_plots=[int(nxy_total*i) for i in plot_fracs] # number of points to plot in each direction for various plots
seeds_per_step=int(gridbox_seeds_per_step*nxy_total*nxy_total) # how many particles to add each time step
substeps=len(substep_lengths) # this is used for moving particles around
time_step=0
subdomain_size=int(nxy_total*subdomain_frac)
subdomain_corr=int(nxy_total*subdomain_frac)**-2
domain_corr=int(nxy_total)**-2
if(abs(sum(substep_lengths)-1.0)>1.0e-6):
     sys.exit("Substep lengths do not add to 1")
     
# Particle positions etc
buffer_length=nxy_total*nxy_total*3*max(n_crit_convect_low-1,n_crit_neighbour_low)
p_x_low=zeros(buffer_length,float32)
p_x_upp=zeros(buffer_length,float32)
p_y_low=zeros(buffer_length,float32)
p_y_upp=zeros(buffer_length,float32)
p_time_upp=ones(buffer_length,uint16)*65535 # time spent in upper layer (initially set to high value)

# Indices that keep track of the number of paricles in each layer 
p_index_low=zeros(1,uint32) # unfortunately, the easiest weave-compatible solution is to use array of length 1
p_index_upp=zeros(1,uint32)

# Rain cluster variables
rain_buffer_length=nxy_total*nxy_total/2 # maximum number of clusters
rain_x=zeros(rain_buffer_length,float32)
rain_y=zeros(rain_buffer_length,float32)
rain_time=zeros(rain_buffer_length,uint32) # current time (could be useful for stats?)
rain_powers=zeros(rain_buffer_length,float32) # scaling factor for velocity
rain_magns=zeros(rain_buffer_length,uint32) # number of parcticles
rain_areas=zeros(rain_buffer_length,uint32) # area of rain cluster in grid cells
rain_inv_powers=zeros(rain_buffer_length,float32)
rain_index=zeros(1,uint32)

# Array with trigger sizes in single time step
trigger_magns=zeros(rain_buffer_length,uint32)
trigger_areas=zeros(rain_buffer_length,uint32)
trigger_index=zeros(1,uint32)

# Rain variables on the intermediate grid
gridbox_buffer_length=nxy_fine*nxy_fine*4+60
coarse_grid_rain_x=zeros((nxy_coarse,nxy_coarse,gridbox_buffer_length),float32)
coarse_grid_rain_y=zeros((nxy_coarse,nxy_coarse,gridbox_buffer_length),float32)
coarse_grid_rain_powers=zeros((nxy_coarse,nxy_coarse,gridbox_buffer_length),float32)
coarse_grid_rain_inv_powers=zeros((nxy_coarse,nxy_coarse,gridbox_buffer_length),float32)
coarse_grid_rain_index=zeros((nxy_coarse,nxy_coarse),uint32)

# Gridded numbers, stored for pyqtgraph visualisation purposes
n_grid_low=zeros((nxy_total,nxy_total),int8)
n_grid_upp=zeros((nxy_total,nxy_total),int8)
n_grid_rain=zeros((nxy_total,nxy_total),int8)

# Arrays to store time-dependent information
# For plotting of time dependent statistics
sum_particles_low=zeros(time_steps,float32)
sum_particles_upp=zeros(time_steps,float32)
sum_subdomain_low=zeros(time_steps,float32)
sum_subdomain_upp=zeros(time_steps,float32)
sum_filled_subdomain_low=zeros(time_steps,float32)
sum_empty_low=zeros(time_steps,float32)
sum_rain_clusters=zeros(time_steps,float32)
sum_subdomain_rain_clusters=zeros(time_steps,float32)
max_rain_cluster_size=zeros(time_steps,uint32)
max_percell_low=zeros(time_steps,uint32)
max_percell_upp=zeros(time_steps,uint32)

# Arrays to store information
# For collecting size statistics
stat_buffer_length=nxy_total*nxy_total*4
trigger_magn_occurrences=zeros(stat_buffer_length,int32)
rain_magns_occurrences=zeros(stat_buffer_length,int32)
trigger_area_occurrences=zeros(stat_buffer_length,int32)
rain_area_occurrences=zeros(stat_buffer_length,int32)

# Use a global support code for bringing math, io and divisions into each piece of weave code
global_support_code="""
    #include <stdio.h>
    #include <math.h>
    #include <stdint.h>

    float const pi= 3.14159265358979323846264338327950288419716939937510f;
    typedef uint16_t uint16t;
    typedef uint32_t uint32t;
    typedef int8_t int8t;
    float const inv_rand_max=1.0f/4294967295.0f;
    
    // functions needed to calculate the modulus for integers and floats
    inline int imod (int a, int b)
    {
      if(b < 0)
        return imod(-a, -b);   
      int ret = a % b;
      if(ret < 0)
        ret+=b;
      return ret;
    }
    
    inline float wmod(float a, float N)
    {
      float ret = a - N * floor (a / N);
      return ret;
    }
    
    uint32_t rand0, rand1, rand2, rand3;
    
    // generates uniform random number
    float generate_uniform(void) {
        uint32_t rand5 = rand0;
        rand5 ^= rand5 << 11;
        rand5 ^= rand5 >> 8;
        rand0 = rand1; rand1 = rand2; rand2 = rand3;
        rand3 ^= rand3 >> 19;
        rand3 ^= rand5;
        return (rand3+0.5f)*inv_rand_max;
    }
    """

# Moves particles with shear, diffuses particles using random displacement
# Takes into account grid periodicity
def diffuse_translate(p_x,p_y,p_index,diffuse_speed,translate_speed,nxy_total):
    # except case without particles
    randarray=np.array([int(10000*i) for i in np.random.random(4)])
    log_me(str(randarray),plotpath+'/log')
    if(p_index[0]<1):
       return
    code = """
    float dist=0.0f,angle=0.0f;
    
    // Initialise uniform random algorithm
    rand0=randarray(0);rand1=randarray(1);rand2=randarray(2);rand3=randarray(3);
    
    if(diffuse_speed>1e-16){
      for (int p=0; p<p_index(0); ++p) {
        dist=2.0f*diffuse_speed*generate_uniform();
        angle=2.0f*pi*generate_uniform();
        p_x(p)=wmod(p_x(p)+translate_speed,nxy_total);
        p_x(p)=wmod((p_x(p)+dist*cos(angle)),nxy_total);
        p_y(p)=wmod((p_y(p)+dist*sin(angle)),nxy_total);
      }
      printf("dist=%f    ",dist);
    }
    else{
      for (int p=0; p<p_index(0); ++p) {
        p_x(p)=wmod(p_x(p)+translate_speed,nxy_total);
      }
    }
    """          
    weave.inline(code,['p_x','p_y','p_index','diffuse_speed','translate_speed','nxy_total','randarray'],
                 type_converters = converters.blitz,support_code=global_support_code)
    
# Remove particles than have exceeded rain duration from upper layer
def delete_particles(p_x,p_y,p_time,p_index):
    code = """
    int p=0;
    int p_index_temp=0;
    for(int p=0;p<p_index(0);++p) {
      if(p_time(p)>time_step-rain_delay-rain_duration) {
        p_x(p_index_temp)=p_x(p);
        p_y(p_index_temp)=p_y(p);
        p_time(p_index_temp)=p_time(p);
        p_index_temp=p_index_temp+1;
      }
    }
    p_index(0)=p_index_temp;
    """          
    weave.inline(code, ['p_x','p_y','p_time','p_index','rain_duration','rain_delay','time_step'],type_converters = converters.blitz)
    
# Determines which rain clusters may influence the particles within a coarse grid box
# First looks for the cluster that gives the smallest upper bound on cluster size weighted distance
# Subsequently adds clusters that have a smaller lower bound on cluster size weighted distance as candidates on the coarse grid
# Execution speed is obtained by "early rejection" of candidate clusters
def rain_to_coarse_grid(rain_x,rain_y,rain_index,rain_powers,rain_inv_powers,rain_time,nxy_coarse,nxy_fine,coarse_grid_rain_x,coarse_grid_rain_y,coarse_grid_rain_powers,coarse_grid_rain_inv_powers,coarse_grid_rain_index,dist_add):
    # Except case without rain
    if(rain_index[0]<1):
        return
    rain_buffer_length=len(rain_x)
    code = """
    int opt_rain_coarse=0; // cluster with lowest upper bound on the coarse grid
    int coarse_index=0; //assign index to each cluster that is a candidate
    float invvel_upper_bound=0.0f,dist_x_test=0.0f,dist_y_test=0.0f,invvel_test=0.0f,current_x=0.0f,current_y=0.0f,invvel_add=0.0f;
    float invvel_x_test=0.0f,invvel_y_test=0.0f;
    int nxy_total;
    float h_fine;
    nxy_total=nxy_fine*nxy_coarse;
    blitz::Array<float,1> invvel_x_test_upperbound(rain_buffer_length);
    blitz::Array<float,1> invvel_x_test_lowerbound(rain_buffer_length);
    invvel_x_test_lowerbound=0.0f;
    invvel_x_test_upperbound=0.0f;
    h_fine=0.5f*nxy_fine+1.0e-6f;
    // calculate inverse cluster size, which comes into the velocity
    for (int rain_nr=0; rain_nr<rain_index(0); ++rain_nr) {
      rain_inv_powers(rain_nr)=(1.0f/rain_powers(rain_nr));
    }
    
    // loop over coarse grid in x direction
    // minimize the inverse velocity (computational constraint)
    for (int coarse_x=0; coarse_x<nxy_coarse; ++coarse_x) {
      current_x=(coarse_x+0.50f)*nxy_fine;
      // determine lower bound on distance for each rain cluster 
      for (int rain_nr=0; rain_nr<rain_index(0); ++rain_nr) {
         // upper bound on distance to each cluster from this cell
         dist_x_test=std::min(fabsf(current_x-rain_x(rain_nr)),nxy_total-fabsf(current_x-rain_x(rain_nr)));
         invvel_x_test_upperbound(rain_nr)=rain_inv_powers(rain_nr)*std::min(dist_x_test+h_fine,0.5f*nxy_total);
         // lower bound on distance to each cluster from this cell
         invvel_x_test_lowerbound(rain_nr)=rain_inv_powers(rain_nr)*std::max(dist_x_test-h_fine,0.0f);
      }

      // loop over coarse grid in y direction
      for (int coarse_y=0; coarse_y<nxy_coarse; ++coarse_y) {
        current_y=(coarse_y+0.5f)*nxy_fine;
        // set placeholder value for smallest distance lower bound (single rain point at full domain length)              
        invvel_upper_bound=nxy_total+dist_add;
        coarse_grid_rain_index(coarse_x,coarse_y)=0;
        opt_rain_coarse=-1;
        // determine optimal rain cluster for cell center, using the lower bounds already present
        for (int rain_nr=0; rain_nr<rain_index(0); ++rain_nr) {            
          invvel_x_test=invvel_x_test_upperbound(rain_nr);
          invvel_add=rain_inv_powers(rain_nr)*dist_add;
          if(invvel_x_test+invvel_add>invvel_upper_bound) {
            continue;
          }
          dist_y_test=std::min(fabsf(current_y-rain_y(rain_nr)),nxy_total-fabsf(current_y-rain_y(rain_nr)));
          invvel_y_test=rain_inv_powers(rain_nr)*std::min(dist_y_test+h_fine,0.5f*nxy_total);
          if(invvel_y_test+invvel_add>invvel_upper_bound) {
            continue;
          }
         invvel_test=sqrt(invvel_x_test*invvel_x_test+invvel_y_test*invvel_y_test)+invvel_add;
          // determine cluster with smallest upper bound on coarse grid
          if(invvel_test<invvel_upper_bound) {
            opt_rain_coarse=rain_nr;
            invvel_upper_bound=invvel_test;
          }
        }
        
        // determine potential optimal clusters for the whole coarse grid
        if(opt_rain_coarse<0) {
           continue;
        }
        for (int rain_nr=0; rain_nr<rain_index(0); ++rain_nr) {
          // lower bound on distance to each cluster in the coarse cell in x-direction
          invvel_add=rain_inv_powers(rain_nr)*dist_add;
          invvel_x_test=invvel_x_test_lowerbound(rain_nr);
          if(invvel_x_test+invvel_add>invvel_upper_bound) {
            continue;
          }
          // lower bound on distance to each cluster in the coarse cell in y-direction
          dist_y_test=std::min(fabsf(current_y-rain_y(rain_nr)),nxy_total-fabsf(current_y-rain_y(rain_nr)));
          invvel_y_test=rain_inv_powers(rain_nr)*std::max(dist_y_test-h_fine,0.0f);
          if(invvel_y_test+invvel_add>invvel_upper_bound) {
            continue;
          }
          // lower bound on total distance for this cluster
          invvel_test=sqrt(invvel_x_test*invvel_x_test+invvel_y_test*invvel_y_test)+invvel_add;
          // check if this is smaller than the upper bound, if so we have identified a candidate
          if(invvel_test<=invvel_upper_bound) {
            coarse_index=coarse_grid_rain_index(coarse_x,coarse_y);
            coarse_grid_rain_x(coarse_x,coarse_y,coarse_index)=rain_x(rain_nr);
            coarse_grid_rain_y(coarse_x,coarse_y,coarse_index)=rain_y(rain_nr);
            coarse_grid_rain_powers(coarse_x,coarse_y,coarse_index)=rain_powers(rain_nr);
            coarse_grid_rain_inv_powers(coarse_x,coarse_y,coarse_index)=rain_inv_powers(rain_nr);
            coarse_grid_rain_index(coarse_x,coarse_y)=coarse_grid_rain_index(coarse_x,coarse_y)+1;
          }
        } // end loop over clusters
                        
      } // end loop over y grid
    } //end loop over x grid
    """          
    weave.inline(code, ['rain_x','rain_y','rain_index','rain_powers','rain_inv_powers','rain_time','time_step','rain_delay','nxy_coarse','nxy_fine','coarse_grid_rain_x','coarse_grid_rain_y','coarse_grid_rain_powers','coarse_grid_rain_inv_powers','coarse_grid_rain_index','rain_buffer_length','dist_add'],
                 type_converters = converters.blitz,support_code=global_support_code)        
    
# Particle position algorithm for lower layer
# Does diffusion and translation, but also moves particles away from closest rain cluster
# There is the possibility to determine the closest rain cluster using smaller-time steps
# This option gives narrower convergence zones (possibly with a size close to the grid scale)
def move_diffuse_translate(p_x,p_y,p_index,coarse_grid_rain_x,coarse_grid_rain_y,coarse_grid_rain_index,coarse_grid_rain_powers,coarse_grid_rain_inv_powers,nxy_coarse,nxy_fine,div_speed_scale,diffuse_speed,translate_speed,dist_add):
    if(p_index[0]<1):
       return
    randarray=np.array([int(10000*i) for i in np.random.random(4)])
    code = """
    int opt_rain_cluster=0;
    float invvel_opt=0.0f,dist_x=0.0f,dist_y=0.0f,invvel=0.0f,substep_length=0.0f,opt_true_dist=0.0f,opt_true_dist_limiter=0.0f,invvel_add=0.0f;
    float invvel_x=0.0f,invvel_y=0.0f;
    float p_x_substep=0.0f,p_y_substep=0.0f;
    float delta_x=0.0f,delta_y=0.0f;
    float opt_cld_x=0.0f,opt_cld_y=0.0f;
    int opt_cld_size=0;
    int nxy_total,x_coarse=0,y_coarse=0;
    nxy_total=nxy_coarse*nxy_fine;
    float diff_dist=0.0f,cosangle=0.0f,sinangle=0.0f;
    float inv_nxy_fine;
    inv_nxy_fine=1.0f/nxy_fine;
    float angle=0.0f; 
    
    // Initialise uniform random algorithm
    rand0=randarray(0);rand1=randarray(1);rand2=randarray(2);rand3=randarray(3);
    
    for (int p=0; p<p_index(0); ++p) {
      // determine diffusion speed (correcting for substepping) and angle for entire time step
      diff_dist=2.0f*diffuse_speed*generate_uniform();
      angle=2.0f*pi*generate_uniform();
      cosangle=cos(angle);
      sinangle=sin(angle);
      
      // obtain previous particle postions
      p_x_substep=p_x(p); 
      p_y_substep=p_y(p);
 
      // start substepping loop
      for (int substep=0; substep<substeps; ++substep) {
        substep_length=substep_lengths(substep);
      
        // first do diffusion and translation
        p_x_substep=wmod((p_x_substep+substep_length*diff_dist*cosangle),nxy_total);
        p_y_substep=wmod((p_y_substep+substep_length*diff_dist*sinangle),nxy_total);
        p_x_substep=wmod(p_x_substep+substep_length*translate_speed,nxy_total);

        // determine grid box corresponding to particle
        x_coarse=imod(int(p_x_substep*inv_nxy_fine),nxy_coarse);
        y_coarse=imod(int(p_y_substep*inv_nxy_fine),nxy_coarse);        

        // find optimal rain cluster (smallest weighted distance)
        // take into account domain periodicity
        invvel_opt=sqrt(nxy_total*nxy_total)+dist_add;
        opt_rain_cluster=-1;
        for (int rain_cluster=0; rain_cluster<coarse_grid_rain_index(x_coarse,y_coarse); ++rain_cluster) {
          dist_x=fabsf(p_x_substep-coarse_grid_rain_x(x_coarse,y_coarse,rain_cluster));
          dist_x=std::min(dist_x,nxy_total-dist_x);
          invvel_x=coarse_grid_rain_inv_powers(x_coarse,y_coarse,rain_cluster)*dist_x;
          invvel_add=coarse_grid_rain_inv_powers(x_coarse,y_coarse,rain_cluster)*dist_add;
          if(invvel_x+invvel_add>invvel_opt) {
            continue;
          }
          dist_y=fabsf(p_y_substep-coarse_grid_rain_y(x_coarse,y_coarse,rain_cluster));
          dist_y=std::min(dist_y,nxy_total-dist_y);          
          invvel_y=coarse_grid_rain_inv_powers(x_coarse,y_coarse,rain_cluster)*dist_y;
          if(invvel_y+invvel_add>invvel_opt) {
            continue;
          }
          invvel=sqrt(invvel_x*invvel_x+invvel_y*invvel_y)+invvel_add;
          if(invvel<invvel_opt) {
            opt_rain_cluster=rain_cluster;
            invvel_opt=invvel;
            opt_true_dist=sqrt(dist_x*dist_x+dist_y*dist_y);
          }
        }

        // only act if rain_clusters present
        if(opt_rain_cluster<0) {
          continue;
        }

        // Time integration is done using the change in accumulated distance in the time step
        // and correcting for the periodic domain
        // 1-dimensional equivalent: dx/dt=a/(x+p)
        // For positive x0 and x>x0: x=sqrt(2*a*delta_t+(x0+p)*(x0+p))-p
                
        // move particles using code below
        opt_cld_x=coarse_grid_rain_x(x_coarse,y_coarse,opt_rain_cluster);
        opt_cld_y=coarse_grid_rain_y(x_coarse,y_coarse,opt_rain_cluster);
        opt_cld_size=coarse_grid_rain_powers(x_coarse,y_coarse,opt_rain_cluster);
        opt_true_dist_limiter=std::max(opt_true_dist,1e-6f);
        delta_x=p_x_substep-opt_cld_x;
        delta_y=p_y_substep-opt_cld_y;
                
        if(fabsf(delta_x)<nxy_total-fabsf(delta_x)) {
            p_x_substep=opt_cld_x+delta_x*(sqrt(2.0f*div_speed_scale*substep_length*opt_cld_size+(opt_true_dist+dist_add)*(opt_true_dist+dist_add))-dist_add)/opt_true_dist_limiter;
            p_x_substep=wmod(p_x_substep,nxy_total);
        }
        else if(delta_x>0.0f) {
            p_x_substep=opt_cld_x+(delta_x-nxy_total)*(sqrt(2.0f*div_speed_scale*substep_length*opt_cld_size+(opt_true_dist+dist_add)*(opt_true_dist+dist_add))-dist_add)/opt_true_dist_limiter;
            p_x_substep=wmod(p_x_substep,nxy_total);
        }
        else {
            p_x_substep=opt_cld_x+(delta_x+nxy_total)*(sqrt(2.0f*div_speed_scale*substep_length*opt_cld_size+(opt_true_dist+dist_add)*(opt_true_dist+dist_add))-dist_add)/opt_true_dist_limiter;
            p_x_substep=wmod(p_x_substep,nxy_total);
        }
        if(fabsf(delta_y)<nxy_total-fabsf(delta_y)) {
            p_y_substep=opt_cld_y+delta_y*(sqrt(2.0f*div_speed_scale*substep_length*opt_cld_size+(opt_true_dist+dist_add)*(opt_true_dist+dist_add))-dist_add)/opt_true_dist_limiter;
            p_y_substep=wmod(p_y_substep,nxy_total);
        }
        else if(delta_y>0.0f) {
            p_y_substep=opt_cld_y+(delta_y-nxy_total)*(sqrt(2.0f*div_speed_scale*substep_length*opt_cld_size+(opt_true_dist+dist_add)*(opt_true_dist+dist_add))-dist_add)/opt_true_dist_limiter;
            p_y_substep=wmod(p_y_substep,nxy_total);
        }
        else {
            p_y_substep=opt_cld_y+(delta_y+nxy_total)*(sqrt(2.0f*div_speed_scale*substep_length*opt_cld_size+(opt_true_dist+dist_add)*(opt_true_dist+dist_add))-dist_add)/opt_true_dist_limiter;
            p_y_substep=wmod(p_y_substep,nxy_total);
        }
      } // end sub-step loop
      
      //replace particle position in array
      p_x(p)=wmod(p_x_substep,nxy_total); 
      p_y(p)=wmod(p_y_substep,nxy_total);
    } //end loop over particles
    printf("diff_dist=%f    ",diff_dist);
    """          
    weave.inline(code, ['p_x','p_y','p_index','coarse_grid_rain_x','coarse_grid_rain_y','coarse_grid_rain_index','coarse_grid_rain_powers','coarse_grid_rain_inv_powers','time_step','rain_delay','nxy_coarse','nxy_fine','div_speed_scale','diffuse_speed','translate_speed','substeps','substep_lengths','randarray','dist_add'],
                 type_converters = converters.blitz,support_code=global_support_code)        
     
# Determines rain clusters
# Which are clusters of precipitating particles
# Grows these from particles in order the particles are sorted
# This prevents a preferential location, and ensures cloud location is fairly consistent
# Also finds center of mass
# A cutoff_area_ratio prevents elongated clouds
def determine_rain_clusters(p_x,p_y,p_time,rain_x,rain_y,p_index,rain_index,rain_powers,rain_magns,rain_areas,rain_time,n_grid,n_grid_active,nxy_total,minsize,n_crit_convect,n_crit_neighbour):
    code = r"""
    int candidate_index=0; //indices to monitor particle deletion
    int rain_part_index=0,added_points_index=0,points_toadd_index=0,this_rain_magn=0;
    int x_grid=0,y_grid=0,x_grid_rain=0,y_grid_rain=0;
    float x_sum_sin_loc=0.0f,x_sum_cos_loc=0.0f,y_sum_sin_loc=0.0f,y_sum_cos_loc=0.0f,x_angle_loc,y_angle_loc=0.0f;
    float inv_nxy_total=0.0f,this_rain_inv_powers=0.0f;
    int x_this_neighbour=0,y_this_neighbour=0;
    int local_buffer_length=nxy_total*nxy_total;
    int rain_buffer_length=nxy_total*nxy_total/4;
    int this_rain_perimeter=0,this_rain_area=0,dead_neighbours=0,tempcand=0;
    
    blitz::Array<uint16t,1> x_neighbour(4);
    blitz::Array<uint16t,1> y_neighbour(4);
    blitz::Array<uint32t,1> candidates(local_buffer_length);
    blitz::Array<uint16t,1> candidates_x_grid(local_buffer_length);
    blitz::Array<uint16t,1> candidates_y_grid(local_buffer_length);
    blitz::Array<uint16t,1> new_rainstart_x(rain_buffer_length);
    blitz::Array<uint16t,1> new_rainstart_y(rain_buffer_length);
    blitz::Array<uint16t,1> stored_rainstart_x(rain_buffer_length);
    blitz::Array<uint16t,1> stored_rainstart_y(rain_buffer_length);
    blitz::Array<bool,2> rain_on_grid(nxy_total,nxy_total);
    blitz::Array<bool,2> toppled_grid(nxy_total,nxy_total);
    blitz::Array<float,2> x_sum_sin_loc_grid(nxy_total,nxy_total);
    blitz::Array<float,2> y_sum_sin_loc_grid(nxy_total,nxy_total);
    blitz::Array<float,2> x_sum_cos_loc_grid(nxy_total,nxy_total);
    blitz::Array<float,2> y_sum_cos_loc_grid(nxy_total,nxy_total);

    x_neighbour=0;
    y_neighbour=0;
    candidates=0;
    candidates_x_grid=0;
    candidates_y_grid=0;
    new_rainstart_x=0;
    new_rainstart_y=0;
    stored_rainstart_x=0;
    stored_rainstart_y=0;
    rain_on_grid=false;
    toppled_grid=false;
    x_sum_sin_loc_grid=0.0f;
    y_sum_sin_loc_grid=0.0f;
    x_sum_cos_loc_grid=0.0f;
    y_sum_cos_loc_grid=0.0f;
    
    // reset rain index and grids
    rain_index(0)=0; 
    n_grid_active=0;
    
    inv_nxy_total=1.0f/nxy_total;
    for (x_grid=0; x_grid<nxy_total; ++x_grid) {
      for (y_grid=0; y_grid<nxy_total; ++y_grid) {
        n_grid(x_grid,y_grid)=0;
        n_grid_active(x_grid,y_grid)=0;
        toppled_grid(x_grid,y_grid)=false;
        rain_on_grid(x_grid,y_grid)=false;
        x_sum_sin_loc_grid(x_grid,y_grid)=0;
        y_sum_sin_loc_grid(x_grid,y_grid)=0;
        x_sum_cos_loc_grid(x_grid,y_grid)=0;
        y_sum_cos_loc_grid(x_grid,y_grid)=0;
      }
    }
    
    // particle list
    for (int p=0; p<p_index(0); ++p) {
      x_grid=imod(int(p_x(p)),nxy_total);
      y_grid=imod(int(p_y(p)),nxy_total);
      n_grid(x_grid,y_grid)=n_grid(x_grid,y_grid)+1;      
    }
   
    candidate_index=0;
    // loop over particles
    for (int p=0; p<p_index(0); ++p) {
      if(p_time(p) <= time_step-rain_delay) {
        x_grid=imod(int(p_x(p)),nxy_total);
        y_grid=imod(int(p_y(p)),nxy_total);
        n_grid_active(x_grid,y_grid)=n_grid_active(x_grid,y_grid)+1;
        if(n_grid_active(x_grid,y_grid)==n_crit_convect) { //only one particle per critical n_grid cell
          candidates(candidate_index)=p; 
          candidate_index=candidate_index+1;
        }
        x_sum_sin_loc_grid(x_grid,y_grid)=x_sum_sin_loc_grid(x_grid,y_grid)+sin(2.0f*pi*wmod(p_x(p),nxy_total)*inv_nxy_total);
        x_sum_cos_loc_grid(x_grid,y_grid)=x_sum_cos_loc_grid(x_grid,y_grid)+cos(2.0f*pi*wmod(p_x(p),nxy_total)*inv_nxy_total);
        y_sum_sin_loc_grid(x_grid,y_grid)=y_sum_sin_loc_grid(x_grid,y_grid)+sin(2.0f*pi*wmod(p_y(p),nxy_total)*inv_nxy_total);
        y_sum_cos_loc_grid(x_grid,y_grid)=y_sum_cos_loc_grid(x_grid,y_grid)+cos(2.0f*pi*wmod(p_y(p),nxy_total)*inv_nxy_total);          
      }
    }
    
    // loop over candidate particles
    for(int candidate=0; candidate<candidate_index; ++candidate) {
      // exclude cells which have been toppled already
      tempcand=candidates(candidate);
      x_grid=imod(int(p_x(tempcand)),nxy_total);
      y_grid=imod(int(p_y(tempcand)),nxy_total);
                 
      if(toppled_grid(x_grid,y_grid)==true) { 
        continue;
      }
      
      // initialise growth algorithm
      stored_rainstart_x(0)=x_grid;
      stored_rainstart_y(0)=y_grid;
      added_points_index=1;
      rain_part_index=0;
      this_rain_magn=0;
      
      // add own cell contents to rain cluster
      candidates_x_grid(rain_part_index)=x_grid;
      candidates_y_grid(rain_part_index)=y_grid;
      rain_part_index=rain_part_index+1;
      this_rain_magn=this_rain_magn+n_grid_active(x_grid,y_grid);
      toppled_grid(x_grid,y_grid)=true;

      // Recursive cluster identification; check on number of iterations prevents elongated clusters    
      this_rain_perimeter=4;
      this_rain_area=1;
      while((added_points_index>0) && (this_rain_perimeter*this_rain_perimeter<cutoff_area_ratio*cutoff_area_ratio*16*this_rain_area)) {
        points_toadd_index=0;
        for (int added_point=0; added_point<added_points_index; ++added_point) {
          x_neighbour(0)=imod(stored_rainstart_x(added_point)-1,nxy_total);
          x_neighbour(1)=stored_rainstart_x(added_point);
          x_neighbour(2)=stored_rainstart_x(added_point);
          x_neighbour(3)=imod(stored_rainstart_x(added_point)+1,nxy_total);
          y_neighbour(0)=stored_rainstart_y(added_point);
          y_neighbour(1)=imod(stored_rainstart_y(added_point)-1,nxy_total);
          y_neighbour(2)=imod(stored_rainstart_y(added_point)+1,nxy_total);
          y_neighbour(3)=stored_rainstart_y(added_point);
          for (int direction=0; direction<4; ++direction) {
            x_this_neighbour=x_neighbour(direction);
            y_this_neighbour=y_neighbour(direction);
            // exclude already toppled_grid cells again
            if((n_grid_active(x_this_neighbour,y_this_neighbour)>(n_crit_neighbour-1)) && (toppled_grid(x_this_neighbour,y_this_neighbour)==false)) {
              new_rainstart_x(points_toadd_index)=x_this_neighbour;
              new_rainstart_y(points_toadd_index)=y_this_neighbour;
              points_toadd_index=points_toadd_index+1;
              // add particles on affected n_grid cell to updated candidates list
              candidates_x_grid(rain_part_index)=x_this_neighbour;
              candidates_y_grid(rain_part_index)=y_this_neighbour;
              rain_part_index=rain_part_index+1;
              this_rain_magn=this_rain_magn+n_grid_active(x_this_neighbour,y_this_neighbour);
              this_rain_area=this_rain_area+1;
              toppled_grid(x_this_neighbour,y_this_neighbour)=true;
              dead_neighbours=0;
              if (toppled_grid(imod(x_this_neighbour+1,nxy_total),y_this_neighbour)==false) {
                 dead_neighbours=dead_neighbours+1;
              }
              if (toppled_grid(imod(x_this_neighbour-1,nxy_total),y_this_neighbour)==false) {
                 dead_neighbours=dead_neighbours+1;
              }
              if (toppled_grid(x_this_neighbour,imod(y_this_neighbour+1,nxy_total))==false) {
                 dead_neighbours=dead_neighbours+1;
              }
              if (toppled_grid(x_this_neighbour,imod(y_this_neighbour-1,nxy_total))==false) {
                 dead_neighbours=dead_neighbours+1;
              }              
              this_rain_perimeter=this_rain_perimeter-4+2*dead_neighbours;
            }
          }
        }
        for (int point_toadd=0; point_toadd<points_toadd_index; ++point_toadd) {
          stored_rainstart_x(point_toadd)=new_rainstart_x(point_toadd);
          stored_rainstart_y(point_toadd)=new_rainstart_y(point_toadd);
        }
        added_points_index=points_toadd_index;
      }
      
      // determine periodic center of mass of each rain cluster
      if(this_rain_magn>minsize) {
        this_rain_inv_powers=1.0f/this_rain_magn;
        x_sum_sin_loc=0.0f;
        x_sum_cos_loc=0.0f;
        y_sum_sin_loc=0.0f;
        y_sum_cos_loc=0.0f;
        for (int rain_part=0; rain_part<rain_part_index; ++rain_part) {
          x_grid_rain=candidates_x_grid(rain_part);
          y_grid_rain=candidates_y_grid(rain_part);
          x_sum_sin_loc=x_sum_sin_loc+x_sum_sin_loc_grid(x_grid_rain,y_grid_rain);
          x_sum_cos_loc=x_sum_cos_loc+x_sum_cos_loc_grid(x_grid_rain,y_grid_rain);
          y_sum_sin_loc=y_sum_sin_loc+y_sum_sin_loc_grid(x_grid_rain,y_grid_rain);
          y_sum_cos_loc=y_sum_cos_loc+y_sum_cos_loc_grid(x_grid_rain,y_grid_rain);
          rain_on_grid(x_grid_rain,y_grid_rain)=true;
        }
        x_angle_loc=atan2(x_sum_sin_loc*this_rain_inv_powers,x_sum_cos_loc*this_rain_inv_powers);
        y_angle_loc=atan2(y_sum_sin_loc*this_rain_inv_powers,y_sum_cos_loc*this_rain_inv_powers);
        rain_x(rain_index(0))=wmod(nxy_total*x_angle_loc/(2.0f*pi),nxy_total);
        rain_y(rain_index(0))=wmod(nxy_total*y_angle_loc/(2.0f*pi),nxy_total);
        rain_powers(rain_index(0))=sqrt(this_rain_magn-minsize);
        rain_magns(rain_index(0))=this_rain_magn;
        rain_areas(rain_index(0))=this_rain_area;
        rain_time(rain_index(0))=time_step;
        rain_index(0)=rain_index(0)+1;
      }
    } // master loop over candidate particles
        """        
    weave.inline(code, ['p_x','p_y','p_time','rain_x','rain_y','n_grid','n_grid_active','p_index','rain_index','rain_powers','rain_areas','rain_time','nxy_total','time_step','n_crit_convect','n_crit_neighbour','minsize', 'rain_duration','rain_delay','cutoff_area_ratio','rain_magns'],
                 type_converters = converters.blitz,support_code=global_support_code)

# This determines triggering clusters
# Method similar to above
# No limits on triggering cluster shapes at present
def trigger_swap(p_x_low,p_y_low,p_x_upp,p_y_upp,p_time_upp,p_index_low,p_index_upp,n_grid_low,nxy_total,minsize_low,n_crit_convect_low,n_crit_neighbour_low,trigger_magns,trigger_areas,trigger_index):
    global time_step
    code = r"""
    int candidate_index=0; //indices to monitor particle deletion
    int trigger_part_index=0,added_points_index=0,points_toadd_index=0,trigger_magn=0;
    int x_grid=0,y_grid=0,x_grid_trigger=0,y_grid_trigger=0;
    int x_this_neighbour=0,y_this_neighbour=0;
    int this_trigger_area=0;
    int p=0;

    int local_buffer_length=nxy_total*nxy_total;
    int trigger_buffer_length=nxy_total*nxy_total/4;
    blitz::Array<uint32t,1> candidates(local_buffer_length);
    blitz::Array<uint16t,1> candidates_x_grid(local_buffer_length);
    blitz::Array<uint16t,1> candidates_y_grid(local_buffer_length);
    blitz::Array<uint16t,1> new_triggerstart_x(trigger_buffer_length);
    blitz::Array<uint16t,1> new_triggerstart_y(trigger_buffer_length);
    blitz::Array<uint16t,1> stored_triggerstart_x(trigger_buffer_length);
    blitz::Array<uint16t,1> stored_triggerstart_y(trigger_buffer_length);
    blitz::Array<bool,2> trigger_on_grid(nxy_total,nxy_total);
    blitz::Array<bool,2> toppled_grid(nxy_total,nxy_total);
    blitz::Array<uint16t,1> x_neighbour(4);
    blitz::Array<uint16t,1> y_neighbour(4);

    candidates=0;
    candidates_x_grid=0;
    candidates_y_grid=0;
    new_triggerstart_x=0;
    new_triggerstart_y=0;
    stored_triggerstart_x=0;
    stored_triggerstart_y=0;
    trigger_on_grid=false;
    toppled_grid=false;
    x_neighbour=0;
    y_neighbour=0;
            
    // reset counting and toppling grids
    for (x_grid=0; x_grid<nxy_total; ++x_grid) {
      for (y_grid=0; y_grid<nxy_total; ++y_grid) {
        n_grid_low(x_grid,y_grid)=0;
        toppled_grid(x_grid,y_grid)=false;
        trigger_on_grid(x_grid,y_grid)=false;
      }
    }
    
    // loop over particles
    // only one critical particle per grid cell
    candidate_index=0;
    for (int p=0; p<p_index_low(0); ++p) {
      x_grid=imod(int(p_x_low(p)),nxy_total);
      y_grid=imod(int(p_y_low(p)),nxy_total);
      n_grid_low(x_grid,y_grid)=n_grid_low(x_grid,y_grid)+1;
      if(n_grid_low(x_grid,y_grid)==n_crit_convect_low) { 
        candidates(candidate_index)=p; 
        candidate_index=candidate_index+1;
      }       
    }
    
    // loop over candidate particles
    trigger_index(0)=0;
    for (int candidate=0; candidate<candidate_index; ++candidate) {
      // exclude cells which have been toppled already
      x_grid=imod(int(p_x_low(candidates(candidate))),nxy_total);
      y_grid=imod(int(p_y_low(candidates(candidate))),nxy_total);
      if(toppled_grid(x_grid,y_grid)==true) {
        continue;
      }

      // initialise growth algorithm
      stored_triggerstart_x(0)=x_grid;
      stored_triggerstart_y(0)=y_grid;
      added_points_index=1;
      trigger_part_index=0;
      trigger_magn=0;
      this_trigger_area=0;

      // add own cell contents to trigger
      candidates_x_grid(trigger_part_index)=x_grid;
      candidates_y_grid(trigger_part_index)=y_grid;
      trigger_part_index=trigger_part_index+1;
      trigger_magn=trigger_magn+n_grid_low(x_grid,y_grid);
      toppled_grid(x_grid,y_grid)=true;
      
      // Recursive cluster identification algorithm
      while(added_points_index>0) {
        points_toadd_index=0;
        for (int added_point=0; added_point<added_points_index; ++added_point) {
          x_neighbour(0)=imod(stored_triggerstart_x(added_point)-1,nxy_total);
          x_neighbour(1)=stored_triggerstart_x(added_point);
          x_neighbour(2)=stored_triggerstart_x(added_point);
          x_neighbour(3)=imod(stored_triggerstart_x(added_point)+1,nxy_total);
          y_neighbour(0)=stored_triggerstart_y(added_point);
          y_neighbour(1)=imod(stored_triggerstart_y(added_point)-1,nxy_total);
          y_neighbour(2)=imod(stored_triggerstart_y(added_point)+1,nxy_total);
          y_neighbour(3)=stored_triggerstart_y(added_point);
          for (int neighbour=0; neighbour<4; ++neighbour) {
            x_this_neighbour=x_neighbour(neighbour);
            y_this_neighbour=y_neighbour(neighbour);
            // exclude already toppled_grid cells again
            if((n_grid_low(x_this_neighbour,y_this_neighbour)>(n_crit_neighbour_low-1)) && (toppled_grid(x_this_neighbour,y_this_neighbour)==false)) {
              new_triggerstart_x(points_toadd_index)=x_this_neighbour;
              new_triggerstart_y(points_toadd_index)=y_this_neighbour;
              points_toadd_index=points_toadd_index+1;
              // add particles on affected n_grid cell to updated candidates list
              candidates_x_grid(trigger_part_index)=x_this_neighbour;
              candidates_y_grid(trigger_part_index)=y_this_neighbour;
              trigger_part_index=trigger_part_index+1;
              this_trigger_area=this_trigger_area+1;
              trigger_magn=trigger_magn+n_grid_low(x_this_neighbour,y_this_neighbour);
              toppled_grid(x_this_neighbour,y_this_neighbour)=true;
            }
          }
        }
        for (int point_toadd=0; point_toadd<points_toadd_index; ++point_toadd) {
          stored_triggerstart_x(point_toadd)=new_triggerstart_x(point_toadd);
          stored_triggerstart_y(point_toadd)=new_triggerstart_y(point_toadd);
        }
        added_points_index=points_toadd_index;
      }
      
      // mark cell for triggering
      if(trigger_magn>minsize_low) {
        trigger_magns(trigger_index(0))=trigger_magn;
        trigger_areas(trigger_index(0))=this_trigger_area;
        trigger_index(0)=trigger_index(0)+1;
        for (int trigger_part=0; trigger_part<trigger_part_index; ++trigger_part) {
          x_grid_trigger=candidates_x_grid(trigger_part);
          y_grid_trigger=candidates_y_grid(trigger_part);
          trigger_on_grid(x_grid_trigger,y_grid_trigger)=true;
        }
      }
    } // master loop over candidate particles
    
    // truely move particles to upper grid
    int p_index_temp=0;
    for(int p=0;p<p_index_low(0);++p) {
      x_grid=imod(int(p_x_low(p)),nxy_total);
      y_grid=imod(int(p_y_low(p)),nxy_total);
      if (trigger_on_grid(x_grid,y_grid)==true) {
        p_x_upp(p_index_upp(0))=p_x_low(p);
        p_y_upp(p_index_upp(0))=p_y_low(p);
        p_time_upp(p_index_upp(0))=time_step;
        p_index_upp(0)=p_index_upp(0)+1;
      }
      else{
        p_x_low(p_index_temp)=p_x_low(p);
        p_y_low(p_index_temp)=p_y_low(p);
        p_index_temp=p_index_temp+1;      
      }
    }
    p_index_low(0)=p_index_temp;
    """          
    weave.inline(code, ['p_x_low','p_y_low','p_x_upp','p_y_upp','p_time_upp','n_grid_low','p_index_low','p_index_upp','nxy_total','time_step','n_crit_convect_low','n_crit_neighbour_low','minsize_low','trigger_magns','trigger_areas','trigger_index'],
                 type_converters = converters.blitz,support_code=global_support_code)

# forced makedir
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

# print and save
def log_me(my_input,my_outfile):
    print my_input
    with open(my_outfile,'a+') as outfile:
       outfile.write(my_input)
       outfile.write("\n")
       outfile.close()
        
def adjust_field_plots(nxy_plot):
    plot([0,0,subdomain_size,subdomain_size,0],[0,subdomain_size,subdomain_size,0,0],linewidth=2,c='k')
    xlim(0,nxy_plot)
    ylim(0,nxy_plot)
    xlabel('x')
    ylabel('y')
    gca().set_position([0.1,0.1,0.8,0.8])
    
def draw_fields_simple(nxy_plot):
    if(mod(time_step,plot_every)>0):
       return
    if(not use_mpl_simple):
       return
    nr_p_low=p_index_low[0]
    nr_p_upp=p_index_upp[0]
    nr_cld=rain_index[0]
    scalenr=12000.0/(nxy_plot*nxy_plot)
        
    figure(figsize=(6,6))
    norm=matplotlib.colors.Normalize(vmin = 0, vmax = n_crit_convect_low*1.1, clip = True)
    im = NonUniformImage(gca(),cmap=my_cmap,interpolation='nearest',extent=(0,nxy_plot,0,nxy_plot),norm = norm)
    im.set_data(arange(nxy_plot)+0.5, arange(nxy_plot)+0.5,transpose(n_grid_low[0:nxy_plot,0:nxy_plot]))
    gca().images.append(im)
    if l_color_plots: 
        scatter(rain_x[0:nr_cld],rain_y[0:nr_cld],s=scalenr*rain_magns[0:nr_cld],c='#004d80',edgecolor='none',alpha=0.8)
    else:
        scatter(rain_x[0:nr_cld],rain_y[0:nr_cld],s=scalenr*rain_magns[0:nr_cld],c='none',edgecolor='k')        
    adjust_field_plots(nxy_plot)
    title('lower layer particles, rain clusters  \n')
    plotname=plotpath+'/simple_lower_%(#0)05d_%(#1)05d.' %{"#0":nxy_plot,"#1":time_step}+outformat
    savefig(plotname)
    if not(l_color_plots) and outformat=='png':
         img=Image.open(plotname).convert('L')
         img.save(plotname)
    close()
        
    figure(figsize=(6,6))
    norm=matplotlib.colors.Normalize(vmin = 0, vmax = n_crit_convect_low*1.1, clip = True)
    im = NonUniformImage(gca(),cmap=my_cmap,interpolation='nearest',extent=(0,nxy_plot,0,nxy_plot),norm = norm)
    im.set_data(arange(nxy_plot)+0.5, arange(nxy_plot)+0.5,transpose(sqrt(n_crit_convect_low)*sqrt(n_grid_rain[0:nxy_plot,0:nxy_plot])))
    gca().images.append(im)
    if l_color_plots: 
        scatter(rain_x[0:nr_cld],rain_y[0:nr_cld],s=scalenr*rain_magns[0:nr_cld],c='#004d80',edgecolor='none',alpha=0.8)
    else:
        scatter(rain_x[0:nr_cld],rain_y[0:nr_cld],s=scalenr*rain_magns[0:nr_cld],c='none',edgecolor='k')
    adjust_field_plots(nxy_plot)
    title('rain particles, rain clusters \n')
    plotname=plotpath+'/simple_upper_%(#0)05d_%(#1)05d.' %{"#0":nxy_plot,"#1":time_step}+outformat
    savefig(plotname)
    if not(l_color_plots) and outformat=='png':
         img=Image.open(plotname).convert('L')
         img.save(plotname)
    close()
                                
def draw_fields_detailed(nxy_plot):
    if(mod(time_step,plot_every_detailed)>0):
       return
    if(not use_mpl_detailed):
       return
    nr_p_low=p_index_low[0]
    nr_p_upp=p_index_upp[0]
    nr_cld=rain_index[0]
    
    figure(figsize=(6,6))
    if l_color_plots:
        scatter(p_x_low[0:nr_p_low],p_y_low[0:nr_p_low],s=50000.0/(nxy_plot*nxy_plot),c='#b30000',edgecolor='None')
        scatter(rain_x[0:nr_cld],rain_y[0:nr_cld],s=12000.0*rain_magns[0:nr_cld]/(nxy_plot*nxy_plot),c='#004d80',edgecolor='None',alpha=0.8)
    else:
        scatter(rain_x[0:nr_cld],rain_y[0:nr_cld],s=12000.0*rain_magns[0:nr_cld]/(nxy_plot*nxy_plot),c='None',edgecolor='k')      
        scatter(p_x_low[0:nr_p_low],p_y_low[0:nr_p_low],s=50000.0/(nxy_plot*nxy_plot),c='0.5',edgecolor='None',alpha=0.5)
    adjust_field_plots(nxy_plot)
    title('lower layer particles, rain clusters  \n')
    plotname=plotpath+'/lower_%(#0)05d_%(#1)05d.' %{"#0":nxy_plot,"#1":time_step}+outformat
    savefig(plotname)
    if not(l_color_plots) and outformat=='png':
         img=Image.open(plotname).convert('L')
         img.save(plotname)
    close()
    
    figure(figsize=(6,6))
    raining=(0*p_time_upp<99)
    raining[0:nr_p_upp]=(p_time_upp[0:nr_p_upp]< time_step-rain_delay)
    if l_color_plots:
        scatter(p_x_upp[raining],p_y_upp[raining],s=50000.0/(nxy_plot*nxy_plot),c='#000000',edgecolor='None')
        scatter(p_x_upp[logical_not(raining)],p_y_upp[logical_not(raining)],s=50000.0/(nxy_plot*nxy_plot),c='#739900',edgecolor='None')
        scatter(rain_x[0:nr_cld],rain_y[0:nr_cld],s=12000.0*rain_magns[0:nr_cld]/(nxy_plot*nxy_plot),c='#004d80',edgecolor='None',alpha=0.8)
    else:
        scatter(rain_x[0:nr_cld],rain_y[0:nr_cld],s=12000.0*rain_magns[0:nr_cld]/(nxy_plot*nxy_plot),c='none',edgecolor='k')
        scatter(p_x_upp[logical_not(raining)],p_y_upp[logical_not(raining)],50000.0/(nxy_plot*nxy_plot),c='0.75',edgecolor='None')
        scatter(p_x_upp[raining],p_y_upp[raining],s=50000.0/(nxy_plot*nxy_plot),c='0.3',edgecolor='None')      
        scatter(rain_x[0:nr_cld],rain_y[0:nr_cld],s=12000.0*rain_magns[0:nr_cld]/(nxy_plot*nxy_plot),c='none',edgecolor='k',alpha=0.5)
    adjust_field_plots(nxy_plot)
    title('upper layer particles (rain/non-rain), rain clusters \n')
    plotname=plotpath+'/upper_%(#0)05d_%(#1)05d.' %{"#0":nxy_plot,"#1":time_step}+outformat
    savefig(plotname)
    if not(l_color_plots) and outformat=='png':
         img=Image.open(plotname).convert('L')
         img.save(plotname)
    close()
            
def draw_stats():
    if(mod(time_step,plot_every)>0):
        return
    if not(use_mpl_stats):
        return
    if(time_step>=time_steps):
        return
    figure(figsize=(12,3))   
    l1,l2,l3,l4,l5=plot(range(time_step),sum_particles_low[0:time_step],range(time_step),sum_particles_upp[0:time_step],range(time_step),sum_subdomain_low[0:time_step],range(time_step),sum_subdomain_upp[0:time_step],range(time_step),sum_filled_subdomain_low[0:time_step],linewidth=2)
    l3.set_dashes((6,2),)
    l4.set_dashes((6,2),)
    l5.set_dashes((2,2),)
    annotate('time = %(#)6d'%{"#": time_step},xy=(0.02, 1.05), xycoords='axes fraction',fontsize=14)
    annotate('particle statistics',xy=(0.85, 1.05), xycoords='axes fraction',fontsize=14)
    legend(('<n$_{low}$>','<n$_{upp}$>','subdomain <n$_{low}$>','subdomain <n$_{upp}$>','subdomain <n$_{low} \\neq$0>'))
    xlim(max(0,time_step-400),max(time_step+100,200))
    ylim(0,n_crit_convect_low*0.75)
    xlabel('time')
    gca().xaxis.set_ticks([max(0,time_step-400),max(time_step+100,200)]) 
    gca().set_position([0.05,0.08,0.9,0.8])
    plotname=plotpath+'/stats_%(#)05d.' %{"#": time_step}+outformat
    savefig(plotname)
    if not(l_color_plots) and outformat=='png':
         img=Image.open(plotname).convert('L')
         img.save(plotname)
    close()
    
    # combine plots using PIL
    def combineplots(movie_path,lower_path,upper_path,stats_path):
        figure(figsize=(12,9))   
        savefig(movie_path)
        close()
        movie_im=Image.open(movie_path)
        lower_im=Image.open(lower_path)
        upper_im=Image.open(upper_path)
        stats_im=Image.open(stats_path)
        x_offset = lower_im.size[0]
        y_offset = lower_im.size[1]
        movie_im.paste(lower_im, (0,0))
        movie_im.paste(upper_im, (x_offset,0))
        movie_im.paste(stats_im, (0,y_offset))
        movie_im.save(movie_path)
        if not(l_color_plots) and outformat=='png':
            img=Image.open(movie_path).convert('L')
            img.save(movie_path)
            
    if use_mpl_detailed and (mod(time_step,plot_every_detailed)==0) and outformat=='png':
        for nxy_plot in nxy_plots:
            movie_path=plotpath+'/movie_%(#0)05d_%(#1)05d.' %{"#0":nxy_plot,"#1":time_step}+outformat
            lower_path=plotpath+'/lower_%(#0)05d_%(#1)05d.' %{"#0":nxy_plot,"#1":time_step}+outformat
            upper_path=plotpath+'/upper_%(#0)05d_%(#1)05d.' %{"#0":nxy_plot,"#1":time_step}+outformat
            stats_path=plotpath+'/stats_%(#)05d.' %{"#":time_step}+outformat
            combineplots(movie_path,lower_path,upper_path,stats_path)

    if use_mpl_simple and outformat=='png':
        for nxy_plot in nxy_plots:
            movie_path=plotpath+'/simple_movie_%(#0)05d_%(#1)05d.' %{"#0":nxy_plot,"#1":time_step}+outformat
            lower_path=plotpath+'/simple_lower_%(#0)05d_%(#1)05d.' %{"#0":nxy_plot,"#1":time_step}+outformat
            upper_path=plotpath+'/simple_upper_%(#0)05d_%(#1)05d.' %{"#0":nxy_plot,"#1":time_step}+outformat
            stats_path=plotpath+'/stats_%(#)05d.' %{"#":time_step}+outformat
            combineplots(movie_path,lower_path,upper_path,stats_path)
            
def update_stats():
    if(time_step>=time_steps):
        return
    sum_particles_low[time_step]=domain_corr*p_index_low[0]
    sum_particles_upp[time_step]=domain_corr*p_index_upp[0]
    sum_empty_low[time_step]=domain_corr*sum([n_grid_low==0])
    sum_rain_clusters[time_step]=domain_corr*rain_index[0]
    sum_subdomain_low[time_step]=subdomain_corr*sum(n_grid_low[0:subdomain_size,0:subdomain_size]) # subdomain variability
    sum_subdomain_upp[time_step]=subdomain_corr*sum(n_grid_upp[0:subdomain_size,0:subdomain_size])
    sum_filled_subdomain_low[time_step]=subdomain_corr*sum(n_grid_low[0:subdomain_size,0:subdomain_size]>0)
    sum_subdomain_rain_clusters[time_step]=0.0
    for rain_nr in range(rain_index[0]):
        if(rain_x[rain_nr]<int(nxy_total*subdomain_frac)):
            if(rain_y[rain_nr]<int(nxy_total*subdomain_frac)):
                sum_subdomain_rain_clusters[time_step]=sum_subdomain_rain_clusters[time_step]+domain_corr
    max_rain_cluster_size[time_step]=0
    if(rain_index[0]>0):
        max_rain_cluster_size[time_step]=max(rain_magns[0:rain_index[0]])
    max_percell_low[time_step]=nanmax(n_grid_low)
    max_percell_upp[time_step]=nanmax(n_grid_upp)

def size_statistics():
    global trigger_magn_occurrences,trigger_area_occurrences,rain_magns_occurrences,rain_area_occurrences
    if not(use_mpl_stats):
        return
    def add_to_size_counts(freq_arr,array_in,index):
        count_occurrences=np.bincount(array_in[0:index])
        freq_arr[0:len(count_occurrences)]=freq_arr[0:len(count_occurrences)]+count_occurrences
        return freq_arr
    add_to_size_counts(trigger_magn_occurrences,trigger_magns,trigger_index[0])
    add_to_size_counts(trigger_area_occurrences,trigger_areas,trigger_index[0])
    add_to_size_counts(rain_magns_occurrences,rain_magns,rain_index[0])
    add_to_size_counts(rain_area_occurrences,rain_areas,rain_index[0])
    def plot_size_dist(freq_arr,file_prefix,my_title,xlim_discretisation):
        normalised_freq=trim_zeros(freq_arr,trim='b')/float(nxy_total*nxy_total*size_stats_write_every)
        # log-lin plot
        figure(figsize=(6,4))
        plot(range(len(normalised_freq)),log(normalised_freq)/log(10.))
        xlabel('size')
        ylabel('$^{10}$log(frequency)')
        title(my_title)
        xlim(0,(1+int(len(normalised_freq)/xlim_discretisation)) * xlim_discretisation)
        ylim(-9,-3)
        gca().set_position([0.175,0.15,0.7,0.7])
        plotname=plotpath+'/'+file_prefix+'_%(#)05d.'%{"#": time_step}+outformat
        savefig(plotname)
        if not(l_color_plots) and outformat=='png':
            img=Image.open(plotname).convert('L')
            img.save(plotname)
        close()
        # log-log plot
        figure(figsize=(6,4))        
        plot(log(range(len(normalised_freq)))/log(10.),log(normalised_freq)/log(10.))
        xlabel('$^{10}$log(size)')
        ylabel('$^{10}$log(frequency)')
        title(my_title)
        xlim(1.5,3.5)
        ylim(-9,-3)
        gca().set_position([0.175,0.15,0.7,0.7])
        plotname=plotpath+'/log_'+file_prefix+'_%(#)05d.'%{"#": time_step}+outformat
        savefig(plotname)
        if not(l_color_plots) and outformat=='png':
            img=Image.open(plotname).convert('L')
            img.save(plotname)
        close()        
        np.savetxt(plotpath+'/'+file_prefix+'_%(#)05d.txt' %{"#": time_step},normalised_freq,delimiter='\n')
        
        freq_arr[0:len(normalised_freq)]=0        
        return freq_arr
    if(mod(time_step,size_stats_write_every)==0):
        plot_size_dist(trigger_magn_occurrences,'trigger_magn','normalised $M_t$ distribution histogram \n',50)
        plot_size_dist(trigger_area_occurrences,'trigger_area','normalised $A_t$ distribution histogram \n',20)
        plot_size_dist(rain_magns_occurrences,'rain_power','normalised $M$ distribution histogram \n',50)
        plot_size_dist(rain_area_occurrences,'rain_area','normalised $A$ distribution histogram \n',20)
        
def update_PyQt():
    if(not use_pyqtgraph):
         return
    global img1, img2
    img1.setImage(n_grid_low)
    img1.setLevels([0,n_crit_convect_low*1.1])
    img2.setImage(sqrt(n_grid_rain))
    img2.setLevels([0,n_crit_convect_low*1.1])
    QtCore.QTimer.singleShot(1, integrate_tstep)
                        
def integrate_tstep():
    global time_step
    minsize_upp=0
    if(time_step>time_grow_start):
        minsize_upp=minsize_end*min((time_step-time_grow_start)/float(time_to_grow),1.0)
        log_me( "minsize_upp "+str(minsize_upp),plotpath+'/log')
    if((l_disturb_run==True) and (time_step==1000)):
        rain_nr=1
        rain_x[rain_nr]=mod(rain_x[rain_nr]+1.0e-2,nxy_total)
        log_me('disturb:qing at'+str(rain_x[rain_nr])+' ,'+str(rain_y[rain_nr]),plotpath+'/log')
    seeds_this_step=np.random.poisson(seeds_per_step)
    p_x_low[p_index_low[0]:p_index_low[0]+seeds_this_step]=nxy_total*np.random.random(seeds_this_step)
    p_y_low[p_index_low[0]:p_index_low[0]+seeds_this_step]=nxy_total*np.random.random(seeds_this_step)
    p_index_low[0]=p_index_low[0]+seeds_this_step
    rain_to_coarse_grid(rain_x,rain_y,rain_index,rain_powers,rain_inv_powers,rain_time,nxy_coarse,nxy_fine,coarse_grid_rain_x,coarse_grid_rain_y,coarse_grid_rain_powers,coarse_grid_rain_inv_powers,coarse_grid_rain_index,dist_add)
    move_diffuse_translate(p_x_low,p_y_low,p_index_low,coarse_grid_rain_x,coarse_grid_rain_y,coarse_grid_rain_index,coarse_grid_rain_powers,coarse_grid_rain_inv_powers,nxy_coarse,nxy_fine,div_speed_scale,diffuse_speed_low,translate_speed_low,dist_add)
    diffuse_translate(p_x_upp,p_y_upp,p_index_upp,diffuse_speed_upp,translate_speed_upp,nxy_total)
    delete_particles(p_x_upp,p_y_upp,p_time_upp,p_index_upp)
    trigger_swap(p_x_low,p_y_low,p_x_upp,p_y_upp,p_time_upp,p_index_low,p_index_upp,n_grid_low,nxy_total,minsize_low,n_crit_convect_low,n_crit_neighbour_low,trigger_magns,trigger_areas,trigger_index)
    determine_rain_clusters(p_x_upp,p_y_upp,p_time_upp,rain_x,rain_y,p_index_upp,rain_index,rain_powers,rain_magns,rain_areas,rain_time,n_grid_upp,n_grid_rain,nxy_total,minsize_upp,n_crit_convect_upp,n_crit_neighbour_upp)
    for nxy_plot in nxy_plots:
        draw_fields_detailed(nxy_plot)
        draw_fields_simple(nxy_plot)
    update_stats()
    draw_stats()
    size_statistics()
    log_me('time_step,rain_index[0],rain_x[0],rain_y[0],rain_magns[0]',plotpath+'/log')
    log_me(str(time_step)+','+str(rain_index[0])+','+str(rain_x[0])+','+str(rain_y[0])+','+str(rain_magns[0]),plotpath+'/log')
    time_step=time_step+1
    log_me('time='+str(time.time()-start_time),plotpath+'/log')
    if(time_step==time_steps):
        np.savez_compressed(plotpath+'/sum_particles_low.npz',sum_particles_low)
        np.savez_compressed(plotpath+'/sum_particles_upp.npz',sum_particles_upp)
        np.savez_compressed(plotpath+'/sum_subdomain_low.npz',sum_subdomain_low)
        np.savez_compressed(plotpath+'/sum_subdomain_upp.npz',sum_subdomain_upp)
        np.savez_compressed(plotpath+'/sum_filled_subdomain_low.npz',sum_filled_subdomain_low)
        np.savez_compressed(plotpath+'/sum_empty_low.npz',sum_empty_low)
        np.savez_compressed(plotpath+'/sum_rain_clusters.npz',sum_rain_clusters)
        np.savez_compressed(plotpath+'/sum_subdomain_rain_clusters.npz',sum_subdomain_rain_clusters)
        np.savez_compressed(plotpath+'/max_rain_cluster_size.npz',max_rain_cluster_size)
        np.savez_compressed(plotpath+'/max_percell_low.npz',max_percell_low)
        np.savez_compressed(plotpath+'/max_percell_upp.npz',max_percell_upp)
    update_PyQt()
    

def cmap_map(function,cmap):
    """ Applies function (which should operate on vectors of shape 3:
    [r, g, b], on colormap cmap. This routine will break any discontinuous
    points in a colormap.
    """
    cdict = cmap._segmentdata
    step_dict = {}
    # Firt get the list of points where the segments start or end
    for key in ('red','green','blue'):         step_dict[key] = map(lambda x: x[0], cdict[key])
    step_list = reduce(lambda x, y: x+y, step_dict.values())
    step_list = array(list(set(step_list)))
    # Then compute the LUT, and apply the function to the LUT
    reduced_cmap = lambda step : array(cmap(step)[0:3])
    old_LUT = array(map( reduced_cmap, step_list))
    new_LUT = array(map( function, old_LUT))
    # Now try to make a minimal segment definition of the new LUT
    cdict = {}
    for i,key in enumerate(('red','green','blue')):
        this_cdict = {}
        for j,step in enumerate(step_list):
            if step in step_dict[key]:
                this_cdict[step] = new_LUT[j,i]
            elif new_LUT[j,i]!=old_LUT[j,i]:
                this_cdict[step] = new_LUT[j,i]
        colorvector=  map(lambda x: x + (x[1], ), this_cdict.items())
        colorvector.sort()
        cdict[key] = colorvector
    return matplotlib.colors.LinearSegmentedColormap('colormap',cdict,1024)

def init_plots():
    if(os.path.isdir(plotpath)):
       shutil.rmtree(plotpath)
    mkdir_p(plotpath)
    with open(plotpath+'/log', 'w') as f:
       f.close()
    shutil.copy('voronoi_efficient.py',plotpath) #copy self to plot directory...so the code is always there
      
if(use_mpl_simple or use_mpl_detailed or use_mpl_stats):
    almostblack='#262626'
    params = {'axes.edgecolor': almostblack,
              'axes.labelcolor': almostblack,
              'text.color': almostblack, 
              'xtick.color': almostblack, 
              'ytick.color': almostblack,
              'ps.usedistiller': 'none',
              'pdf.fonttype' : 42,
              'axes.labelsize': 14,
              'figure.dpi': 150,
              'savefig.dpi': 150,
              'font.size': 11,
              'axes.titlesize': 14,
              'legend.fontsize': 11,
              'xtick.labelsize': 11,
              'ytick.labelsize': 11,
              }
    rcParams.update(params)
    if l_color_plots:
        CBcdict={'a1':'#262626','a2':'#A37935','a3':'#6CCD54','a4':'#00295F','a5':'#EA1F00','a6':'#BD97E2',}
        my_cmap='OrRd'
    else:
        CBcdict={'a1':'k','a2':'0.4','a3':'k','a4':'0.4','a5':'k','a6':'0.6',}
        my_cmap=cmap_map(lambda x: 0.4 + 0.55*x, cm.Greys)      
    matplotlib.rcParams['axes.prop_cycle'] = (cycler('color',[CBcdict[c] for c in 'a1','a2','a3','a4','a5','a6']))
  
if(use_pyqtgraph):
    app=QtGui.QApplication([])
    win=pg.GraphicsLayoutWidget()
    win.show()  ## show widget alone in its own window
    win.setWindowTitle('ColdPools (PyQt)')
    view1 = win.addViewBox()
    view2 = win.addViewBox()
    img1 = pg.ImageItem(border='w')
    img2 = pg.ImageItem(border='w')   
    for view,img in zip([view1,view2],[img1,img2]):
        ## lock the aspect ratio so pixels are always square
        view.setAspectLocked(True)
        ## Create image item
        view.addItem(img)
        ## Set initial view bounds
        view.setRange(QtCore.QRectF(0, 0, nxy_total, nxy_total))
    updateTime = pg.ptime.time()

start_time=time.time()       
## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__' and use_pyqtgraph:
    init_plots()
    integrate_tstep()
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
elif(l_run_all_exps):
    plotpaths=          ['64coarse','16coarse' ,'coarse','fine','4coarse','8coarse','32coarse','128coarse','160coarse','320coarse']
    minsize_ends=       [30        ,30         ,30      ,30    ,30       ,30       ,30        ,30         ,30         ,30         ]  
    nxy_coarses=        [64        ,16         ,1       ,640   ,4        ,8        ,32        ,128        ,160        ,320        ]
    nxy_fines=          [10        ,40         ,640     ,1     ,160      ,80       ,20        ,5          ,4          ,2          ]
    for ppindex in range(len(plotpaths)):
        plotpath=basepath+plotpaths[ppindex]
        minsize_end=minsize_ends[ppindex]
        nxy_coarse=nxy_coarses[ppindex]
        nxy_fine=nxy_fines[ppindex]
        start_time=time.time()
        init_plots()

        #flush buffers and set random seeds
        np.random.seed(random_seed)       
        nxy_total=nxy_coarse*nxy_fine # number of n_grid_low cells in each direction
        nxy_plots=[int(nxy_total*i) for i in plot_fracs] # number of points to plot in each direction for various plots
        seeds_per_step=int(gridbox_seeds_per_step*nxy_total*nxy_total) # how many particles to add each time step
        subdomain_size=int(nxy_total*subdomain_frac)
        subdomain_corr=int(nxy_total*subdomain_frac)**-2
        domain_corr=int(nxy_total)**-2
        time_step=0
           
        # Particle positions etc
        buffer_length=nxy_total*nxy_total*3*max(n_crit_convect_low-1,n_crit_neighbour_low)
        p_x_low=zeros(buffer_length,float32)
        p_x_upp=zeros(buffer_length,float32)
        p_y_low=zeros(buffer_length,float32)
        p_y_upp=zeros(buffer_length,float32)
        p_time_upp=ones(buffer_length,uint16)*65535 # time spent in upper layer (initially set to high value)
        
        # Indices that keep track of the number of paricles in each layer 
        p_index_low=zeros(1,uint32) # unfortunately, the easiest weave solution is to use array of length 1
        p_index_upp=zeros(1,uint32)

        # Rain cluster variables
        rain_buffer_length=nxy_total*nxy_total/2
        rain_x=zeros(rain_buffer_length,float32)
        rain_y=zeros(rain_buffer_length,float32)
        rain_time=zeros(rain_buffer_length,uint32)
        rain_powers=zeros(rain_buffer_length,float32)
        rain_magns=zeros(rain_buffer_length,uint32)
        rain_areas=zeros(rain_buffer_length,uint32)
        rain_inv_powers=zeros(rain_buffer_length,float32)
        rain_index=zeros(1,uint32)
        
        # Array with trigger sizes in single time step
        trigger_magns=zeros(rain_buffer_length,uint32)
        trigger_areas=zeros(rain_buffer_length,uint32)
        trigger_index=zeros(1,uint32)
        
        # Rain variables on the grid
        gridbox_buffer_length=nxy_fine*nxy_fine*4+60
        coarse_grid_rain_x=zeros((nxy_coarse,nxy_coarse,gridbox_buffer_length),float32)
        coarse_grid_rain_y=zeros((nxy_coarse,nxy_coarse,gridbox_buffer_length),float32)
        coarse_grid_rain_powers=zeros((nxy_coarse,nxy_coarse,gridbox_buffer_length),float32)
        coarse_grid_rain_inv_powers=zeros((nxy_coarse,nxy_coarse,gridbox_buffer_length),float32)
        coarse_grid_rain_index=zeros((nxy_coarse,nxy_coarse),uint32)
        
        # Gridded numbers, stored for pyqtgraph visualisation purposes
        n_grid_low=zeros((nxy_total,nxy_total),int8)
        n_grid_upp=zeros((nxy_total,nxy_total),int8)
        n_grid_rain=zeros((nxy_total,nxy_total),int8)
        
        # Arrays to store time-dependent information
        # For plotting of time dependent statistics
        sum_particles_low=zeros(time_steps,float32)
        sum_particles_upp=zeros(time_steps,float32)
        sum_subdomain_low=zeros(time_steps,float32)
        sum_subdomain_upp=zeros(time_steps,float32)
        sum_filled_subdomain_low=zeros(time_steps,float32)
        sum_empty_low=zeros(time_steps,float32)
        sum_rain_clusters=zeros(time_steps,float32)
        sum_subdomain_rain_clusters=zeros(time_steps,float32)
        max_rain_cluster_size=zeros(time_steps,uint32)
        max_percell_low=zeros(time_steps,uint32)
        max_percell_upp=zeros(time_steps,uint32)
        
        # Arrays to store information
        # For collecting size statistics
        stat_buffer_length=nxy_total*nxy_total*4
        trigger_magn_occurrences=zeros(stat_buffer_length,int32)
        rain_magns_occurrences=zeros(stat_buffer_length,int32)
        trigger_area_occurrences=zeros(stat_buffer_length,int32)
        rain_area_occurrences=zeros(stat_buffer_length,int32)
        while(time_step<=time_steps):
            integrate_tstep()
            #dump fields to study disturbance growth
            if(plotpaths[ppindex] in ['low_threshold','perturb'] and time_step in range(1000,1500)):
                np.savez_compressed(plotpath+'/n_grid_low_%(#)05d.npz' %{"#": time_step},n_grid_low)
                np.savez_compressed(plotpath+'/n_grid_upp_%(#)05d.npz' %{"#": time_step},n_grid_upp)
                np.savez_compressed(plotpath+'/n_grid_rain_%(#)05d.npz' %{"#": time_step},n_grid_rain)
                np.savez_compressed(plotpath+'/rain_x_%(#)05d.npz' %{"#": time_step},rain_x)
                np.savez_compressed(plotpath+'/rain_y_%(#)05d.npz' %{"#": time_step},rain_y)
                np.savez_compressed(plotpath+'/rain_m_%(#)05d.npz' %{"#": time_step},rain_powers)
                np.savez_compressed(plotpath+'/p_x_low_%(#)05d.npz' %{"#": time_step},p_x_low)
                np.savez_compressed(plotpath+'/p_x_upp_%(#)05d.npz' %{"#": time_step},p_x_upp)
                np.savez_compressed(plotpath+'/p_y_low_%(#)05d.npz' %{"#": time_step},p_y_low)
                np.savez_compressed(plotpath+'/p_y_upp_%(#)05d.npz' %{"#": time_step},p_y_upp)
else:
    init_plots()
    while(time_step<=time_steps):
        integrate_tstep()
