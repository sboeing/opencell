# OPEN CELLULAR CONVECTION USING LAGRANGIAN TRIGGERING
# A TWO LAYER SIMPLE MODEL WHICH IMITATES CONVECTIVE BEHAVIOUR

# This is a fast implementation using weave
# Uses a double grid for calculating the nearest rain cluster
# The domain can be up to a few thousand points in each direction

# Copyright (c) 2016, University of Leeds
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of University of Leeds nor the
#   names of other contributors may be used to endorse or promote products
# derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL UNIV. OF LEEDS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# IMPORTS
import numpy as np
from numpy import * # Lazy import of numpy functions
from matplotlib.pyplot import * # Lazy import of plotting functions
from matplotlib.colors import Normalize
from matplotlib.image import NonUniformImage
import Image # Use pil to combine plots
import os,errno,sys,shutil

# forced makedir
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise
             
def draw_fields_diff(file1,file2,outfile,intitle,small):
    figure(figsize=(6,6))
    data1=np.load(file1)
    data2=np.load(file2)
    field1=data1[data1.files[0]]
    field2=data2[data2.files[0]]
    diff=field1-field2
    nxy_plot=shape(diff)[0]
    norm=matplotlib.colors.Normalize(vmin = -5, vmax = 5, clip = True)
    im = NonUniformImage(gca(),cmap=my_cmap,interpolation='nearest',extent=(0,nxy_plot,0,nxy_plot),norm = norm)
    im.set_data(arange(nxy_plot), arange(nxy_plot),transpose(diff[0:nxy_plot,0:nxy_plot]))
    gca().images.append(im)
    if small:
        xlim(80,280)
        ylim(360,560)
    else:
        xlim(0,nxy_plot)
        ylim(0,nxy_plot)
    xlabel('x')
    ylabel('y')    
    gca().set_position([0.1,0.1,0.8,0.8])
    title(intitle)
    savefig(outfile,dpi=150)
    close()
    img=Image.open(outfile).convert('L')
    img.save(outfile)
                                                    
def cmap_map(function,cmap):
    """ Applies function (which should operate on vectors of shape 3:
    [r, g, b], on colormap cmap. This routine will break any discontinuous
    points in a colormap.
    """
    cdict = cmap._segmentdata
    step_dict = {}
    # Firt get the list of points where the segments start or end
    for key in ('red','green','blue'):         step_dict[key] = map(lambda x: x[0], cdict[key])
    step_list = reduce(lambda x, y: x+y, step_dict.values())
    step_list = array(list(set(step_list)))
    # Then compute the LUT, and apply the function to the LUT
    reduced_cmap = lambda step : array(cmap(step)[0:3])
    old_LUT = array(map( reduced_cmap, step_list))
    new_LUT = array(map( function, old_LUT))
    # Now try to make a minimal segment definition of the new LUT
    cdict = {}
    for i,key in enumerate(('red','green','blue')):
        this_cdict = {}
        for j,step in enumerate(step_list):
            if step in step_dict[key]:
                this_cdict[step] = new_LUT[j,i]
            elif new_LUT[j,i]!=old_LUT[j,i]:
                this_cdict[step] = new_LUT[j,i]
        colorvector=  map(lambda x: x + (x[1], ), this_cdict.items())
        colorvector.sort()
        cdict[key] = colorvector
    return matplotlib.colors.LinearSegmentedColormap('colormap',cdict,1024)

      
almostblack='#262626'
params = {'axes.edgecolor': almostblack,
              'axes.labelcolor': almostblack,
              'text.color': almostblack, 
              'xtick.color': almostblack, 
              'ytick.color': almostblack,
              'ps.usedistiller': 'none',
              'pdf.fonttype' : 42,
              'axes.labelsize': 14,
              'figure.dpi': 150,
              'savefig.dpi': 150,
              'font.size': 11,
              'axes.titlesize': 14,
              'legend.fontsize': 11,
              'xtick.labelsize': 11,
              'ytick.labelsize': 11,
              }
rcParams.update(params)
CBcdict={'a1':'k','a2':'0.4','a3':'k','a4':'0.4','a5':'k','a6':'0.6',}
my_cmap=cmap_map(lambda x: 0.4 + 0.55*x, cm.Greys)
matplotlib.rcParams['axes.prop_cycle'] = (cycler('color',[CBcdict[c] for c in 'a1','a2','a3','a4','a5','a6']))

mkdir_p('voronoi/distplots' )
for time_step in range(1000,1500):
    for field in ['low','upp','rain']:
        file1='voronoi/low_threshold/n_grid_'+field+'_%(#)05d.npz'%{"#": time_step}
        file2='voronoi/perturb/n_grid_'+field+'_%(#)05d.npz'%{"#": time_step}
        outfile='voronoi/distplots/diff_'+field+'_%(#)05d.png'%{"#": time_step}
        outfile2='voronoi/distplots/small_diff_'+field+'_%(#)05d.png'%{"#": time_step}
        intitle='difference in $n_{%(#)s}$, time= '%{"#": field}+str(time_step)
        draw_fields_diff(file1,file2,outfile,intitle,False)
        draw_fields_diff(file1,file2,outfile2,intitle,True)
