**LICENSE**

*3-clause BSD

*See voronoi_sizeover.py

*Copyright (C) 2015-2016 Steven Boeing, University of Leeds*

**CONTACT**

S."lastname with oe" (at) leeds.ac.uk
OR
sjboing (at) "the usual g-mail suffix"

**DESCRIPTION**

This is code that belongs to the draft
"An object-based model for convective cold pool dynamics"

This directory includes the following scripts
* voronoi_sizeover.py: main code as single file
* voronoi_efficient.py: code copy for testing different domain decompostions
* voronoi_diff.py: example code for difference plots

**REQUIREMENTS**

Python with numpy and weave (older versions of scipy have weave as a subpackage of scipy), matplotlib, PIL (the conda package is called pillow) and optionally pyqtgraph

**OUTPUT FILES**

Are saved as figures (png by deafult), txt and npz (compressed numpy arrays) 

**KNOWN ISSUES/FEATURES**

* Currently not optimised for very large domains (see suggestions in draft)
* Code is mostly procedural programming

**PARAMETERS** 

Are set in voronoi_sizeover.py, and include

use_mpl_stats: make matplotlib statistics plots

use_mpl_simple: make matplotlib plots with gridded particles

use_mpl_detailed: make expensive maplotlib plots

use_pyqtgraph: visualise a single simulation with pyqtgraph

l_color_plots: make color plots

l_run_all_exps: run a suite experiments, like the one used in the draft

See further voronoi_sizeover.py

**EXAMPLE USAGE**

python voronoi_sizeover.py
